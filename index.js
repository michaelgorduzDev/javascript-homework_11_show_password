
const openEye = document.querySelector("#eyeOpen");
const password = document.querySelector("#password");
const closedEye = document.querySelector("#eyeClose");
const eyeOpenConfirm = document.querySelector("#eyeOpenConfirm");
const passwordConfirm = document.querySelector("#password-confirm");
const eyeCloseConfirm = document.querySelector("#eyeCloseConfirm");
let confirmLabel = document.querySelector("#confirm-label");
let p = document.createElement("p");

const container = document.querySelector(".password-form");
container.addEventListener("click", (event) => {
    const target = event.target;
    if (target === openEye || target === closedEye) {
      showPassword(password, openEye, closedEye);
    } else if (target === eyeOpenConfirm || target === eyeCloseConfirm) {
      showPassword(passwordConfirm, eyeOpenConfirm, eyeCloseConfirm);
    }
});

function showPassword(passwordField, iconOpen, iconClose) {

    if (passwordField.type === "password") {
        passwordField.setAttribute("type", "text");
        iconOpen.classList.add("hide");
        iconClose.style.display = "block";
    } else {
        passwordField.setAttribute("type", "password");
        iconOpen.classList.remove("hide");
        iconClose.style.display = "none";
    }
};

const btn = document.querySelector(".btn");
btn.addEventListener("click", (e) => {
    e.preventDefault;
    if (password.value.trim() === '') {
        window.alert("Потрібно ввести значення");
    }
    else if (password.value === passwordConfirm.value) {
        if (confirmLabel.contains(p)) {
            confirmLabel.removeChild(p);
        }
        setTimeout(() => {
            window.alert("You are welcome");
        }, 100);
       
    } else {
        if (confirmLabel.contains(p)) {
            // confirmLabel.removeChild(p);
            return;
        }
        confirmLabel.appendChild(p);
        p.innerHTML = "Потрібно ввести однакові значення";
        p.style.color = "red";
        p.style.position = "absolute";
        p.style.bottom = "-30px";
    }
});